
var productos = ["Death Stranding", "Terminator Resistance", "Pokemon Sword", "Dragon Quest XI", "Borderlands 3", "Portatil", "Memoria RAM", "Router Linux", "Sintonizadora TV"];
var imgs = ["images/ps4/estrenos/death.jpg", "images/xbox one/estrenos/terminator.jpg", "images/switch/estrenos/Pokemon.png", "images/switch/estrenos/dragon.jpg", "images/ps4/estrenos/borde.jpg", "img/productos/6.jpg", "img/productos/7.jpg", "img/productos/8.jpg", "img/productos/9.jpg"];
var descs = ["Un viaje para reconectar una sociedad fragmentada. En un futuro cercano, misteriosas explosiones han sacudido al planeta, desencadenando una serie de eventos sobrenaturales conocidos como Death Stranding. El panorama no es prometedor: el mundo está repleto de extraordinarias criaturas espectrales, y el planeta se encuentra al borde de la extinción masiva. Ahora depende de Sam Bridges abrirse camino por el páramo devastado y salvar a los humanos del exterminio inminente.", 
"Revive los sucesos previos a la batalla final que decidirá el destino de la humanidad en su lucha contra las máquinas. Terminator: Resistance es un shooter en primera persona basado en la época de Future War, sobre la cual pudimos descubrir algunos detalles gracias a las legendarias películas TERMINATOR y T2: Judgment Day. Las máquinas están destinadas a la derrota, ¿pero a qué precio?", 
"Este viaje te llevará a la región de Galar, y a sus vastas y exuberantes praderas, pueblos en los que humanos y Pokémon trabajan juntos y a otros muchos lugares cargados de personalidad. Los combates Pokémon son el entretenimiento más popular en Galar. Por toda la región, fans entusiasmados llenan las gradas de los estadios para vitorear a los Líderes de Gimnasio y a los Aspirantes que los desafían. La región está habitada por una gran variedad de Pokémon. ¡Al visitar una nueva región, descubrirás Pokémon nunca vistos! Como Wooloo, Gossifleur, Drednaw o Corviknight.", 
"La serie clásica de juegos de rol sube de nivel en Nintendo Switch. ¡Disfruta de la versión definitiva de Dragon Quest XI. ¿Todo listo para disfrutar allá donde vayas de una gran aventura repleta de personajes memorables, una historia apasionante y mecánicas clásicas de los juegos de rol? La versión definitiva del aclamado juego incluye el mismo contenido que la versión original, pero se han añadido historias específicas de distintos personajes, música orquestal y la posibilidad de alternar entre ver el mundo en HD o al estilo retro de 16 bits. Además, también podrás escuchar el audio en japonés o en inglés.", 
"¡Vuelve el padre de los shooter-looter, con una aventura llena de caos y tropecientas mil armas. Arrasa a tus enemigos y descubre mundos inéditos con uno de los cuatro nuevos buscacámaras, los cazatesoros cabronazos más duros de Borderlands, que podrás personalizar y desarrollar a través de sus distintos árboles de habilidades. Juega solo o con amigos para derribar a adversarios increíbles, hacerte con montones de botín y salvar tu hogar de la secta más cruel de la galaxia.", 
"Portatil", "Memoria RAM", "Router Linux", "Sintonizadora TV"];
var precios = [1200, 1200, 1400, 1200, 1200, 540, 21, 66, 25];

function SaveArticle(id)
{
    localStorage.setItem("id", id);
}

function LoadFormulario()
{
    document.getElementById('nombreProducto').innerHTML = productos[localStorage.getItem("id")];
    document.images["image"].src = imgs[localStorage.getItem("id")];
    document.getElementById("desc").innerHTML = descs[localStorage.getItem("id")] + "<br><br><br> Precio: " + precios[localStorage.getItem("id")];
}