function validar()
{
    var nombre, correo, telefono, direccion, tipotarjeta;
    var cp= new Array();
    nombre=document.getElementById("nombre").value;
    correo=document.getElementById("correo").value;
    telefono=document.getElementById("telefono").value;
    direccion=document.getElementById("direccion").value;
    cp=document.getElementById("cp").value;
    notarjeta=document.getElementById("no.tarjeta").value;
    tipotarjeta=document.getElementById("tipo.tarjeta").value;

    tipotarjeta=tipotarjeta.toLowerCase();


    expresion = /\w+@\w+\.+[a-z]/;

    if(nombre =="" || correo == "" || telefono == "" || direccion=="" || cp =="" || notarjeta == "" || tipotarjeta == "")
    {
        alert("Todos los campos son obligatorios");
        return false;
    }
    else if(nombre.length>50)
    { alert("El nombre es muy largo");
    return false;
    }
    else if(nombre.length>1 && nombre.length<10)
    { alert("verificar nombre");
    return false;
    }
    else if(correo.length>50)
    { alert("El correo es muy largo");
    return false;
    }
    else if(!expresion.test(correo))
    {
        alert("El correo no es válido");
    return false;
    }
    else if(telefono.length!=10)
    { alert("El numero es de 10 digitos");
    return false;
    }
    else if(isNaN(telefono))
    { alert("El telefono ingresado no es valido");
    return false;
    }
    else if(isNaN(notarjeta))
    {
        alert("El número de cuenta introducido no es valido");
        return false;
    }
    else if(cp.length!=5)
    {
        alert("El código postal es de 5 díguitos")
        return false;
    }
    else if(cp[0]==0 && cp[1]==0)
    {
        alert("Ese codigo postal no existe");
        return false;
    }
    else if(notarjeta.length!=16)
    {
        alert("El numero de diguitos de la targeta es de 16");
        return false;
    }
    else if(tipotarjeta== "credito" || tipotarjeta== "tarjeta de credito" || tipotarjeta=="crédito" || tipotarjeta== "tarjeta de crédito" || tipotarjeta=="debito" || tipotarjeta== "tarjeta de debito" || tipotarjeta=="débito" || tipotarjeta== "tarjeta de débito")
    {
        alert("DATOS GUARDADOS");
        return false;
    }
    else
    {
        alert("no existe ese tipo de targeta");
        return false;
    }
}

